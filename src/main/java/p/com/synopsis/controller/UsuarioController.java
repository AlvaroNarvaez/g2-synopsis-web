package p.com.synopsis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import p.com.synopsis.service.UsuarioService;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping("/")
	public String paginaInicial(Model model) {
		model.addAttribute("usuario", usuarioService.obtenerUsuario());
		return "Inicio";
	}
}
