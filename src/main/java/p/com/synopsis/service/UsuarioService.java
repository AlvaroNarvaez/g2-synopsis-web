package p.com.synopsis.service;

import p.com.synopsis.beans.UsuarioWeb;

public interface UsuarioService {
	
	public UsuarioWeb obtenerUsuario();
}
