package p.com.synopsis.service.impl;

import org.springframework.stereotype.Service;

import p.com.synopsis.beans.UsuarioWeb;
import p.com.synopsis.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Override
	public UsuarioWeb obtenerUsuario() {
		UsuarioWeb usuario = new UsuarioWeb("Alberto", "Maldonado", 36);
		return usuario;
	}
	
}
